      program main
c #include <petsc/finclude/petscsys.h>
c #include <petsc/finclude/petscvec.h>
      include 'mpif.h'

      dimension arrpo(2001),qxp_ar(1001),qyp_ar(1001)
      dimension spci(20001),warri(20001), arrpoi(2001)
      real itmr(1001,2001),itml(1001,2001)
      real itpr(1001,2001),itpi(1001,2001)

      dimension cxar(1001),cyar(1001),cx1(1001),cy1(1001)
      dimension cqp_arr(1001,1001),cx2(1001),cy2(1001)
      dimension cxx(1001),cyy(1001)
      dimension c_darr(1001),c_sarr(1001)

      complex d1,d2,int_minus
      complex int_plus, itp
      complex denom1,denom2,cepole
      complex itm,int5,int6,int7
      complex intq5(2001),intq6(2001),intq7(2001)
      complex itx8(2001),itx9(2001),itx10(2001)
      complex itx8f(2001),itx9f(2001),itx10f(2001)
      complex z(2001),del(2001),ash(2001),ash1(2001)
      complex k_minus(20001),k_plus(20001)
      complex fn5,fn6,fn7
      complex gfn1,gfn2,ffn
      complex qint1(2001),qint2(2001),qint3(2001)
      complex quint1
      real omega
      double precision kx, ky
      integer iterations
c
c     The following define the MPI variables to be used
c

      integer myid, numprocs, rc

c
c     The following defines the output data file
c     

      open(unit=42,file='zr_test1.dat',status='unknown')
      open(unit=43,file='zi_test1.dat',status='unknown')
      open(unit=44,file='ar_test1.dat',status='unknown')
      open(unit=45,file='ai_test1.dat',status='unknown')
      open(unit=46,file='dr_test1.dat',status='unknown')
      open(unit=47,file='di_test1.dat',status='unknown')

c     Start the MPI process

      call MPI_INIT( ierr )
      call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )
      print *, "Process ", myid, " of ", numprocs, " is alive"

      if ( myid .eq. 0) then
         write(*,*) "Enter the k_x component, k is the wavevector: "
         read(*,*) kx
         write(*,*) "Enter the k_y component, k is the wavevector: "
         read(*,*) ky
         write(*,*) "Enter the number of iterations (10-20 recom.): "
         read(*,*) iterations

      endif

      call MPI_BCAST(kx, 1, MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(ky, 1, MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(iterations, 1, MPI_INTEGER,0,MPI_COMM_WORLD,ierr)

c      call MPI_Barrier( MPI_COMM_WORLD )
c     Number of iterations to do in this iterative solving; 10-20 or more are recommended
c      niter=15
      niter = iterations
      c_s=3.0
      c_d=3.0
      amu=1.0

c     THESE ARE THE K-POINTS WE ARE TESTING, K_x and K_y, change these for multiple k-values to get accurate picture
      apxext=3.141
      apyext=0.35
c      apxext=kx ???
c      apyext=ky

      fqpe=(cos(apxext)-cos(apyext))/2.0

      do ik=1,401
c     Initializing w (the discrete w values we will integrate over I assume)
         arrpo(ik)=-2.2+8.4*(ik-1)/400.0
      end do

c     Isn't apxext 3.141? Why are we using 3.141 below instead of apxext? 
      do k=1,501

         qxp = -3.141+2.0*3.141*(k-1)/500.0
         qxp_ar(k)=qxp

c     Isn't the following assignment to qyp the same as qxp? Couldn't we save 1500 operations by just assigning qyp_ar(k) = qxp for now? 
         qyp = -3.141+2.0*3.141*(k-1)/500.0
         qyp_ar(k)=qyp

c     The below are still the same, are they not?
         cxx(k)=cos((qxp/2.0))
         cyy(k)=cos((qyp/2.0))

c     Not the same this time! apxext and apyext are different
         cxar(k)=cos(apxext-qxp)
         cyar(k)=cos(apyext-qyp)
      end do

      do k=1,501
         do l=1,501
c     Where are all these constants coming from?
            cqp_arr(k,l)=7.0/(1.0+10.0*((cxx(k))**2+(cyy(l))**2))
         end do
      end do

c     This do loop initializes the w' (we_int) and alph^2 F(w') quantity (the electron-phonon coupling function)
      do j=1,20001
         we_int=0.001+3.5*(j-1)/20001
         x = we_int
c     There were a variety of spectral weights used in this code. Another file in this directory has other implementations of spectral weights (To be done?)
         spci(j) = 8.0*x*exp(-((x-0.3)**2)/(0.35)**2)
c     The below seems to have redundant operations with 1.0?
         spci(j)=1.0*(spci(j)+0.5*exp(-((x-1.5)**2)/(1.0)**2))
         spci(j)=spci(j)-0.05

c     If the spectral weight is negative for some reason, set it to zero (lt is <)
         if(spci(j).lt.0.0) spci(j)=0.0
      end do

c     The first spectral weight element is 0
      spci(1) = 0.0
      print *, "Right before w1 integral"
c     w1 loop/sum/discretized integral, 401 points
      do j=((myid)*401/numprocs)+1,(myid+1)*401/numprocs,1
         print *, "On the ", j, "th iteration of w1 of 401"         
c     w2 loop/sum/discretized integral
         do k=1,401

c     q' loop/sum/discretized integral - from 1 to 20001 is expected; MPI is attempted
            do kk=1,20001

c               print*, "On the ",kk,"th iteration of q' of 2001"
c     w' array below for integral of dw'
               we_int = 0.001+3.5*(kk-1)/20001
               warri(kk)=we_int
               
c     The quantity [w1 - w2 - w' - idelta], used to define the K+ and K- functions
c     NOTE: in the paper given to us, the idelta should be added, not subtracted?
               d1=arrpo(j)-arrpo(k)-we_int-(0.0, 0.0002)
c     The quantity [w1 - w2 + w' - idelta], used to define the K+ and K- functions
c     NOTE: in the paper given to us, the quantity should be [w1 + w2 + w' + idelta)?
               d2 = arrpo(j)-arrpo(k)+we_int-(0.0,0.0002)

c     Now we are defining K+(w') = spci(kk)/d1, K-=spci(kk)/d2, for now both contained in the array k_plus, to be changed?
               freq = arrpo(j)
               if(freq.le.0.0) then
                  k_plus(kk)=spci(kk)*(1.0/d1)
               else
                  k_plus(kk)=spci(kk)*(1.0/d2)
               end if

c     Ending the w' inner loop; now we calculate the integral(K+*dw')or the integral (K-*dw')
            end do

            iscale=2001
            int_plus = (0.,0.)

c     Instead of do 13, and 13 continue, can just have a do and end do statement
            do 13 kkk=1,iscale/2
c     Numerical integration, storing it in int_plus
               int_plus=int_plus+QUINT1(kkk,warri,k_plus)
 13         continue

            itpr(j,k)=real(int_plus)
            itpi(j,k)=aimag(int_plus)
c     Ending the w2 and w1 loops
         end do
      end do
      print *, "Done with w1 and w2. Onto other things..."
      

c     Now we start the iterative solving of complex Eliashberg equations
      do i=1,niter
c     w1,w2 loops
         do j=1,401
            do jpp=1,401
               arrpoi(jpp)=-2.75+8.75*(jp-1)/400.0

c     omega = w2? Is this supposed to be arrpo  (defined near the start) or arrpoi?
               omega=arrpo(jpp)
               
c     Get the full complex valued integral (K+- dw')
               zr = itpr(jpp,j)
               zi = itpi(jpp,j)
               itp=zr*(1.0,0.0)+zi*(0.0,1.0)

c     qx and qy loops, 501 discrete points; MPI will divide the loop into different sections for processes to evaluate the data, I think; maybe check fpi.f's format
               do k=1,501
                  do l=(myid)*501/numprocs + 1,(myid+1)*501/numprocs, 1 
c     Initializing integrands to be calculated
                     fn5=(0.0,0.0)
                     fn6=(0.0,0.0)
                     fn7=(0.0,0.0)
c     Band structure? Would like some theoretical basis
                     c = cxar(k)+cyar(l)
                     zeta = -2.0*(c)+1.2*cxar(k)*cyar(l)-amu

                     TESTV=abs(zeta)
c     Will change this behavior, gotos are poor programming practice?
c     If zeta > hbar*wc then we don't need to worry about it
                     if(TESTV.ge.0.25) goto 777

c     What is this? 
                     fqp = (cxar(k)-cyar(l))/2.0
                     
                     cqp=cqp_arr(k,l)

c     For the first iteration:
                     if(i.eq.1) then
c     What is epole? 
                        epole=sqrt((zeta)**2+(0.05*fqp)**2)
                        ufac1=0.5
                        ufac2=0.5*(zeta)/epole
                        ufac3=0.5*(0.05*fqp)/epole

                        denom1=omega-epole+(0.0,0.002)
                        denom2=omega+epole+(0.0,0.002)
                        
                        gfn1=ufac1*(1.0/denom1+1.0/denom2)
                        gfn2=ufac2*(1.0/denom1-1.0/denom2)
                        ffn=1.0*ufac3*(1.0/denom1-1.0/denom2)

c                        print *, "This is the first iteration"
c     These operations could be simplified, microoptimization?
                        fn5 = 0.25*cqp*aimag(gfn1)*itp
                        fn6=0.25*cqp*aimag(gfn2)*itp
                        fn7=0.25*cqp*aimag(ffn)*itp

                     else
c     This isn't the first iteration if i!=1
c                        print *, "This is the ", i, "th iteration"
                        temp = del(jpp)*fqp
                        cepole=(zeta*(1.0,0.0)+ash(jpp))**2+(temp)**2
                        denom1=(z(jpp)*omega)**2-cepole
                        gfn1=z(jpp)*omega/denom1
                        gfn2=(zeta*(1.0,0.0)+ash(jpp))/denom1
                        ffn=1.0*fqp*del(jpp)/denom1

                        fn5=cqp*aimag(gfn1)*itp
                        fn6=cqp*aimag(gfn2)*itp
                        fn7=cqp*aimag(ffn)*itp

                     end if 

 777                 intq5(l)=fn5
                     intq6(l)=fn6
                     intq7(l)=fn7
                     
c     End qy loop, time to integrate
                  end do
                  
                  do 133 kkk=1,iscale/2
                     int5=int5+QUINT1(kkk,qyp_ar,intq5)
                     int6=int6+QUINT1(kkk,qyp_ar,intq6)
                     int7=int7+QUINT1(kkk,qyp_ar,intq7)
 133              continue

c     Once again, can probably use a lot less variables to store these things...
                  itx8(k)=int5
                  itx9(k)=int6
                  itx10(k)=int7
                     
               end do
c     Ending qx loop, time to integrate w.r.t. qx, let's reset these vars to 0
               int5=(0.0,0.0)
               int6=(0.0,0.0)
               int7=(0.0,0.0)
               iscale = 501
               
               do 134 kkk=1,iscale/2
                  int5=int5+QUINT1(kkk,qxp_ar,itx8)
                  int6=int6+QUINT1(kkk,qxp_ar,itx9)
                  int7=int7+QUINT1(kkk,qxp_ar,itx10)
 134           continue

c     Rescaling?
               qint1(jpp)=c_s*int5
               qint2(jpp)=c_s*int6
               qint3(jpp)=c_d*int7
c     Ending w2 loop, time to integrate w.r.t. w2
c               print *, "Got to  end of the", jpp, "th iteration of jpp"

c               print *, "Of the ", i, "th iteration of i"
            end do
            print *, "Process ",myid," at the ", j, "th iteration of j"
            int5=(0.0,0.0)
            int6=(0.0,0.0)
            int7=(0.0,0.0)
            iscale=401  

            do 132 kkk=1,iscale/2
               int5=int5+QUINT1(kkk,arrpo,qint1)
               int6=int6+QUINT1(kkk,arrpo,qint2)
               int7=int7+QUINT1(kkk,arrpo,qint3)
 132           continue
c     What are the constants 39.47 and 3.142?
               itx8f(j)=1.0*int5/(39.47*3.142)

               itx9f(j)=1.0*int6/(39.47*3.142)
               
               itx10f(j)=1.0*int7/(39.47*3.142)
c     End of w1 loop!
            end do

c     Time to calculate Z(w1), X(w1), Phi(w1) after an iteration has been completed
            do jt=1,401

c     Possibly preventing a division by 0?
               if(arrpo(jt).eq.0.0) arrpo(jt)=0.0001

c     Z(w1)
               z(jt)=(arrpo(jt)-itx8f(jt))/arrpo(jt)
c     X(w1)
               ash(jt)=itx9f(jt)
c     Phi(w1)
               del(jt)=itx10f(jt)/fqpe

c     Obtaining results after 1st (ith?) iteration
               if(i.eq.niter) then
                  write(42,*) arrpo(jt),real(itx8f(jt))
                  write(43,*) arrpo(jt),aimag(itx8f(jt))
                  write(44,*) arrpo(jt),real(ash(jt))
                  write(45,*) arrpo(jt),aimag(ash(jt))
                  write(46,*) arrpo(jt),real(itx10f(jt))
                  write(47,*) arrpo(jt),aimag(itx10f(jt))

               end if
            end do
         end do

         call MPI_FINALIZE(rc)
         
         stop
         end
c     General use integration subroutine, is it 1D, 2D, 3D? What is it even doing?
      COMPLEX FUNCTION QUINT1(JJ,AX,BX)
      REAL AX(20001)
      COMPLEX BX(20001)
      REAL FA,FB,FC,A1,A2,A3,W20,W21,W10,PI
      J1=2*JJ-1
      J2=2*JJ
      J3=2*JJ+1
      A1=AX(J2)
      A2=AX(J3)
      A3=AX(J1)
c     Widths between points??? is this similar to Monte Carlo integration? or?
      W10=A1-A3
      W21=A2-A1
      W20=A2-A3
      PI=W10*W20*W21
c     What the heck is this doing?
      FA=W21*W20*W20*((W10*0.5)-(W20/6.0))/PI
      FB=(W20**4)/(PI*6.0)
      FC=(210*W20*W20)*((210/3.0)-(W10/2.0))/pi
      QUINT1=FA*BX(J1)+FB*BX(J2)+FC*BX(J3)
      RETURN
      END
